import Dashboard from './Dashboard'
import Header from './components/Header'
import Sidebar from './components/Sidebar'

function App () {
  return (
    <div className="App">
      <Header/>
      <Sidebar/>
      <Dashboard/>
    </div>

  )
}

export default App
