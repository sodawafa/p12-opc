import React from 'react'
import './styles/dashboard.css'
import './styles/charts.css'
import PoidChart from './components/PoidChart'
import ObjectifsChart from './components/ObjectifsChart'
import RadarObjectifsChart from './components/RadarObjectifsChart'
import KPIChart from './components/KPIChart'
import Energie from './components/Energie'
import axios from 'axios'
import { URL } from './APIConfig'

class Dashboard extends React.Component {
  state = {
    id: 18,
    userInfos: [],
    keyData: [],
  }

  componentDidMount () {
    axios.get(`${URL}/user/${this.state.id}`)
      .then(res => {
        const data = res.data.data
        this.setState({
          userInfos: data.userInfos,
          keyData: data.keyData,
        })
      }).catch(e=>console.log(e))
  }

  render () {
    return <main>
      <header>
        <h1>Bonjour <span>{this.state.userInfos.firstName}</span></h1>
        <p>Félicitation ! Vous avez explosé vos objectifs hier 👏</p>
      </header>

      <div className={'container'}>
        <div className={'charts-container'}>
          <PoidChart id={this.state.id}/>
          <div className={'wrapper'}>
            <ObjectifsChart id={this.state.id}/>
            <RadarObjectifsChart id={this.state.id}/>
            <KPIChart id={this.state.id}/>
          </div>
        </div>
        <Energie keyData={this.state.keyData}/>
      </div>

    </main>

  }
}

export default Dashboard
