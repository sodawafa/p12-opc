const PORT = 3000
const PROTOCOL = 'http'
const DOMAIN_NAME = 'localhost'
export const URL = (PROTOCOL ? (PROTOCOL + '://') : '') +
  DOMAIN_NAME +
  (PORT ? (':' + PORT) : '')
