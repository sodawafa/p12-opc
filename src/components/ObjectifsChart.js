import React, { PureComponent } from 'react'
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
} from 'recharts'
import axios from 'axios'
import { URL } from '../APIConfig'

class ObjectifsChart extends PureComponent {
  constructor (props) {
    super(props)
    this.state = ({ id: this.props.id, data: [] })
  }

  /**
   *
   * @param day string
   * @param lang string|null
   * @returns {string}
   */
  day2day (day, lang = '') {
    if (lang.toLowerCase() === 'fr') {
      switch (day) {
        case 1 :
          return 'L'
        case 2 :
          return 'M'
        case 3 :
          return 'M'
        case 4 :
          return 'J'
        case 5 :
          return 'V'
        case 6 :
          return 'S'
        case 7 :
          return 'D'
        default:
          return ''
      }
    }
    switch (day) {
      case 1 :
        return 'M'
      case 2 :
        return 'T'
      case 3 :
        return 'W'
      case 4 :
        return 'T'
      case 5 :
        return 'F'
      case 6 :
        return 'S'
      case 7 :
        return 'S'
      default:
        return ''
    }
  }

  componentDidMount () {
    axios.get(`${URL}/user/${this.state.id}/average-sessions`)
      .then(res => {
        const data = res.data.data
        let sessions = data.sessions.map(
          entry => ({
            day: entry.day,
            name: this.day2day(entry.day, 'fr'),
            value: entry.sessionLength,
            unit: 'min',
          }),
        )
        this.setState({
          data: sessions,
        })
        /* console.log('cmc', sessions)*/

      }).catch(e=>console.log(e))
  }

  render () {
    return (
      <ResponsiveContainer width="100%" height="100%"
                           className={'_ObjectifsChart'}>
        <LineChart
          data={this.state.data}
          margin={{
            top: 50,
            right: 0,
            left: 0,
            bottom: 30,
          }}
        >
          <text fill="#d4d4d4" fontSize="16px"
                className="titleChart2">Durée moyenne des sessions
          </text>
          <XAxis dataKey="name"
                 axisLine={false}
                 tickLine={false}
                 tick={{ stroke: '#FFF', strokeWidth: 0.1 }}
                 name="day"
                 dx={5}
                 padding={{ left: 10, right: 15 }}
          />
          <YAxis dataKey={'value'}
                 scale={'auto'}
                 hide={true}
                 padding={{ top: 50 }}
          />
          <Tooltip
            formatter={(value, name, props) => {
              return [
                (value + ' ' + props.payload.unit), false]
            }}
            wrapperStyle={{
              zIndex: 0,
              backgroundColor: '#FFF',
            }}
            contentStyle={{
              backgroundColor: '#FFF',
            }}
            labelStyle={{
              display: 'none',
            }}
            label={null}
            cursor={{ fill: '#A00', stroke: '#A00', strokeWidth: 100 }}
          />
          <Line type="basis"
                dataKey="value"
                stroke="#fff"
                dot={false}
          />
        </LineChart>
      </ResponsiveContainer>
    )
  }
}

export default ObjectifsChart
