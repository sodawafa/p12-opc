import React from 'react'
import icon1 from '../assets/icon/s-i-1.svg'
import icon2 from '../assets/icon/s-i-2.svg'
import icon3 from '../assets/icon/s-i-3.svg'
import icon4 from '../assets/icon/s-i-4.svg'
import '../styles/sidebar.css'

function Header () {
  return (
    <aside className="App-aside">
      <nav>
        <ul>
          <li><a href="/#"><img src={icon1} alt="icon1"/></a></li>
          <li><a href="/#"><img src={icon2} alt="icon2"/></a></li>
          <li><a href="/#"><img src={icon3} alt="icon3"/></a></li>
          <li><a href="/#"><img src={icon4} alt="icon4"/></a></li>
        </ul>

      </nav>
      <p>Copyright, SportSee 2020</p>
    </aside>
  )
}

export default Header


