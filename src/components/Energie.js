import React, { Component } from 'react'
import '../styles/energie.css'

import calorie from '../assets/icon/calories-icon.svg'
import glucides from '../assets/icon/glucides.svg'
import lipide from '../assets/icon/Lipides.svg'
import protien from '../assets/icon/protein-icon (2).svg'

class Energie extends Component {
  constructor (props) {
    super(props)
    this.state = {
      keyData: this.props.keyData,
    }
  }

  render () {
    return (
      <article className={'_Energie'}>
        <div>
          <img src={calorie} alt="Calories"/>
          <span>
            <h2>{this.props.keyData.calorieCount}kCal</h2>
            <p>Calories</p>
          </span>
        </div>
        <div>
          <img src={protien} alt="Proteines"/>
          <span>
            <h2>{this.props.keyData.proteinCount}g</h2>
            <p>Proteines</p>
          </span>
        </div>
        <div>
          <img src={glucides} alt="Glucides"/>
          <span>
            <h2>{this.props.keyData.carbohydrateCount}g</h2>
            <p>Glucides</p>
          </span>
        </div>
        <div>
          <img src={lipide} alt="Lipides"/>
          <span>
            <h2>{this.props.keyData.lipidCount}g</h2>
            <p>Lipides</p>
          </span>
        </div>
      </article>
    )
  }

}

export default Energie


