import React from 'react'
import Logo from './Logo'
import Nav from './Nav'
import '../styles/header.css'

function Header () {

  return (
    <header className="App-header">
      <Logo/>
      <Nav/>
    </header>
  )
}

export default Header


