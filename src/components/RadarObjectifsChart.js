import React, { PureComponent } from 'react'
import {
  Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
  ResponsiveContainer,
} from 'recharts'
import axios from 'axios'
import { URL } from '../APIConfig'

class RadarObjectifsChart extends PureComponent {
  constructor (props) {
    super(props)
    this.state = ({
      id: this.props.id, data: [
        {
          kind: '',
          val: 0,
        },
      ],
    })
  }

  componentDidMount () {
    axios.get(`${URL}/user/${this.state.id}/performance`)
      .then(res => {
        const data = res.data.data
        let dataRadar = data.data.map(
          entry => ({
            kind: data.kind[entry.kind],
            val: entry.value,
          }),
        )
        this.setState({
          data: dataRadar,
        })
      }).catch(e=>console.log(e))
  }

  render () {
    return (
      <ResponsiveContainer width="100%" height="100%"
                           className={'_RadarObjectifsChart'}>
        <RadarChart cx="50%" cy="50%" outerRadius="80%"
                    data={this.state.data}
                    fill="white">
          <PolarGrid/>
          <PolarAngleAxis dataKey="kind"/>
          <PolarRadiusAxis/>
          <Radar name={this.state.id}
                 dataKey="val"
                 stroke="#f00"
                 fill="#f00"
                 fillOpacity={0.6}/>
        </RadarChart>
      </ResponsiveContainer>
    )
  }
}

export default RadarObjectifsChart
