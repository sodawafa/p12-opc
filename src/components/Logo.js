import logoImg from '../assets/logo.png'

export default function Logo () {
  return (
    <img className={'App-logo'} src={logoImg} alt={'SportSee'}/>
  )
}




