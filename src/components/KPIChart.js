import React, { PureComponent } from 'react'
import {
  PieChart,
  Pie,
  Cell,
  Label,
  ResponsiveContainer,
  Legend,
} from 'recharts'
import axios from 'axios'
import { URL } from '../APIConfig'

const COLORS = ['#FF0000', '#fbfbfb']

/**
 *
 * @param viewBox
 * @param value1 [number]
 * @param value2 [unit]
 * @returns {JSX.Element}
 * @constructor
 */
function CustomLabel ({ viewBox, value1, value2 }) {
  const { cx, cy } = viewBox
  return (
    <text x={cx} y={cy} fill="#000"
          fontWeight={'bolder'}
          className="recharts-text recharts-label label-top-1"
          textAnchor="middle" dominantBaseline="central">
      <tspan alignmentBaseline="middle" fontSize="18">{value1}</tspan>
      <tspan fontSize="16">{value2}</tspan>
    </text>
  )
}

class KPIChart extends PureComponent {
  constructor (props) {
    super(props)
    this.state = ({
      id: this.props.id,
      score: 0,
      data: [
        {
          name: '',
          value: 0,
        },
      ],
    })
  }

  componentDidMount () {
    axios.get(`${URL}/user/${this.state.id}`)
      .then(res => {
        const data = res.data.data
        const score = (data.score ? data.score : data.todayScore) * 100
        this.setState({
          score: score,
          data: [
            { name: 'Score', value: (score) },
            { name: '', value: (100 - (score)) },
          ],
        })
      }).catch(e=>console.log(e))
  }

  render () {
    return (
      <ResponsiveContainer width="100%" height="100%" className={'_KPIChart'}>
        <PieChart onMouseEnter={this.onPieEnter}>
          <Pie
            data={this.state.data}
            cx={150}
            cy={120}
            startAngle={180}
            endAngle={-180}
            /*endAngle={0}*/
            innerRadius={70}
            outerRadius={90}
            fill="#f00"
            paddingAngle={0}
            dataKey="value">
            {
              this.state.data && this.state.data.map((entry, index) => (
                <Cell key={`cell-${index}`}
                      fill={COLORS[index % COLORS.length]}/>
              ))
            }
            <Label width={30} position="centerTop"
                   content={<CustomLabel value1={this.state.score}
                                         value2={'%'}/>}>
            </Label>
            <Label value="de votre" position="centerBottom" fill="#74798c"
                   className="label-bottom-1" fontSize="12px"/>
            <Label value="objectif" position="centerBottom" fill="#74798c"
                   className="label-bottom-2" fontSize="12px"/>
          </Pie>
          <Legend verticalAlign={'top'}
                  align={'left'}
                  iconSize={0}
          />
        </PieChart>
      </ResponsiveContainer>

    )
  }
}

export default KPIChart
