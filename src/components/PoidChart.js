import React, { PureComponent } from 'react'
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts'
import axios from 'axios'
import { URL } from '../APIConfig'

class PoidChart extends PureComponent {
  constructor (props) {
    super(props)
    this.state = ({ id: this.props.id, data: [] })
  }

  componentDidMount () {
    axios.get(`${URL}/user/${this.state.id}/activity`)
      .then(res => {
        const data = res.data.data
        /*this.state.data = data.sessions*/
        let sessions = data.sessions.map(
          entry => ({
            day: parseInt((entry.day).slice(8, 10)),
            date: entry.day,
            calories: entry.calories,
            kilogram: entry.kilogram,
          }),
        )
        this.setState({
          data: sessions,
        })
      }).catch(e=>console.log(e))
  }

  render () {
    return (
      <ResponsiveContainer width="100%" height="100%" className={'_PoidChart'}>
        <BarChart
          width={100}
          height={100}
          data={this.state.data}
          maxBarSize={15}
          barCategoryGap={'40%'}
          barGap="10"
          padding={{ top: 10 }}
        >
          <text fill="#000" fontSize="20px"
                className="titleChart1">Activité quotidienne
          </text>
          <CartesianGrid strokeDasharray="5 5" vertical={false}/>
          <XAxis dataKey="day"
                 tickLine={false}
          />
          <YAxis orientation={'right'} dataKey={'kilogram'} scale={'auto'}
                 yAxisId={'y11'} padding={{ top: 100 }}
                 tickLine={false} axisLine={false}
          />
          <YAxis hide={true} orientation={'left'} dataKey={'calories'}
                 scale={'auto'} yAxisId={'y12'} padding={{ top: 100 }}/>
          <Tooltip
            formatter={(value) => {
              /**
               * formatter : parameters(value, name, props)
               */
              return [(value), false]
            }}
            itemStyle={{
              color: '#fff',
              backgroundColor: '#F00',
            }}
            wrapperStyle={{
              backgroundColor: '#F00',
            }}
            contentStyle={{
              backgroundColor: 'transparent',
            }}
            labelStyle={{
              display: 'none',
            }}
          />
          <Legend verticalAlign={'top'}
                  align={'right'}
                  iconType={'circle'}
                  formatter={(value, name) => {
                    const newValue = (value === 'kilogram')
                      ? 'Poids'
                      : 'Calories brûlées'
                    return [
                      (newValue + '(' + name.payload.unit + ')'), false]
                  }}
          />
          <Bar dataKey="kilogram" fill={'#282D30'} radius={[100, 100, 0, 0]}
               yAxisId={'y11'}
               unit="kg"/>

          <Bar dataKey="calories" fill={'#E60000'} radius={[100, 100, 0, 0]}
               yAxisId={'y12'}
               unit="kcal"/>
        </BarChart>
      </ResponsiveContainer>
    )
  }
}

export default PoidChart
